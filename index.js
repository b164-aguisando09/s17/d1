console.log("hello world");

//arrays & indexes



//array methods

//mutator methods

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

//push()
/*
adds an element in the end of an array and return array length
	syntax:
		arrayName.push();

*/

console.log("Current Array");
console.log(fruits);

let fruitLength = fruits.push('Mango');
console.log(fruitLength);
console.log(fruits);

//add elements
fruits.push("Avocado", 'Guava');
console.log(fruits);

//pop()

/*
removes the last element in an array and returns removed element
	syntax:
		arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);

fruits.pop();
console.log(fruits);

//unshift();
/*
-adds one or more elements at the beginning of an array
	syntax:
		arrayName.unshift();
*/

fruits.unshift('Lime', "Banana");
console.log(fruits);

//shift();
/*
removes an element at the beginning and returns the remove element.
	syntax:
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);

//splice
/*
simultaniously removes element from a specified index number and adds elements
	syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log(fruits);

//sort();
/*
Rearranges the array elements in alphanumeric order
	syntax:
		arrayName.sort();
*/

fruits.sort();
console.log("sort method:");
console.log(fruits);

//reverse();
/*
reverses the order of array elements
	syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log('reverse method');
console.log(fruits);

//Non-Mutator methods
	//methods that are functions that do not modify or change an array after they're created.
	//it returns elements from an array and combining arrays and printing the output.

let countries = ['US', 'PH', 'CAN', 'SG', "PH", 'TH', "FR", 'DE'];

//indexOf();

let firstIndex = countries.indexOf('PH');
console.log(`Result of indexOf method: ${firstIndex}`);

let invalidCountry = countries.indexOf("br");
console.log(`indexOf: ${invalidCountry}`)

//lastIndexOf();

let lastIndex = countries.lastIndexOf("PH");
console.log(`lastIndexOf method: ${lastIndex}`);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log(`lastIndexOf: ${lastIndexStart}`);

//slice();

let slicedArrayA = countries.slice(2);
console.log("slice method:");
console.log(slicedArrayA);
console.log(countries)


let slicedArrayB = countries.slice(2, 4);
console.log("slice method:");
console.log(slicedArrayB);


let slicedArrayC = countries.slice(-3);
console.log("slice method:");
console.log(slicedArrayC);

//toString();

let stringArray = countries.toString();
console.log(stringArray);

//concat()

let tasksArrayA = ['drink HTML', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breath sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log(tasks);

//combine multiple arrays
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

//combine arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log(combinedTasks);

//join();

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join()); //default comma
console.log(users.join(" "));

//iteration methods

//forEach()
/*
similar to a for loop that iterates on each array element

syntax:
	arrayName.forEach(function(indivElement) {
		statement
	})
*/

allTasks.forEach(function(task){
	console.log(task);
});

//miniactivity

for (let i = 0; i <= allTasks.length-1; i++){
	console.log(allTasks[i]);
}

//using forEach with conditional statements
let filteredTasks = [];
 
 allTasks.forEach(function(task){
 	if (task.length > 10) {
 		filteredTasks.push(task);
 	}
 })

console.log(filteredTasks);


//map()

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number;
})

console.log(numbers);
console.log(numberMap);

//every()

let allValid = numbers.every(function (number){
	return (number > 0);
})

console.log("every method");
console.log(allValid);

//some();


let someValid = numbers.some(function(number) {
	return (number < 3)
})

console.log("some method")
console.log(someValid);

//combining the returned result from the every/some method may be used in other statements (if else) to perform consecutive results
if(someValid) {
	console.log('Some of numbers in the array are greater than 2');
}


if(allValid) {
	console.log('ALL of numbers in the array are greater than 0');
}



//filter()

/*
- returns new array that contains elements which meets the given condition
- returns an empty array if no elements were found
- useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods

Syntax:
	let/const resultArray = arrayName.filter(function(indivElement) {
		return expression/condition
	})

*/

let filterValid = numbers.filter(function(number) {
	return (number < 3);
})

console.log("filter method: ");
console.log(filterValid);

let nothingFound = numbers.filter(function(number) {
	return (number == 0)
})

console.log(nothingFound);


//Filtering using forEach
let filteredNumbers = [];


numbers.forEach(function(number) {
	if(number < 3) {
		filteredNumbers.push(number);
	}
})

console.log(filteredNumbers)



//another example using the filter

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes('a')
})

console.log(filteredProducts);
//Methods can be "chained" using them one after another
//The result of the first method is used on the second method until all "chained methods have been resolved"

//How chaining resolves in our example:
//1. The "product" element will be converted into all lowercase letters.
//2. the resulting lowercased string is used in the "includes" method






//reduce()
/*
- evaluates elements from left to right and returns/reduces the array into single value

Syntax:
	let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
		return expression/operation
	})

- accumulator paramater in the function stores the result for every iteration of the loop

-currentValue is the current/next element in the array that is evaluated in each iteration of the loop
*/
//[1, 2, 3, 4, 5]
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {
	//track the current iteration count and accumulator/currentValue data
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);


	//the operation to reduce the array into a single value
	return x + y
})

console.log("result of reduce method: " + reducedArray);


//reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y) {
	return x + ' ' + y;
})

console.log("reduce method: " + reducedJoin);

//Multidimensional arrays
